
INSTALL
-------

1. Copy the shoutcast.module to the Drupal modules directory.

2. Download the shoutcast server software from http://www.shoutcast.com/download/serve.phtml
   (http://www.shoutcast.com/downloads/sc1-9-8/sc_serv_1.9.8_Linux.tar.gz)

3. Unpack sc_serv_1.9.8_Linux.tar.gz to /modules/shoutcast/shoutcast/
   Filesystem should look like:
      sites/default/modules/shoutcast/CHANGELOG.txt
      sites/default/modules/shoutcast/INSTALL.txt
      sites/default/modules/shoutcast/README.txt
      sites/default/modules/shoutcast/shoutcast.module
      sites/default/modules/shoutcast/shoutcast.install
      sites/default/modules/shoutcast/shoutcast/README.TXT
      sites/default/modules/shoutcast/shoutcast/sc_serv
      sites/default/modules/shoutcast/shoutcast/sc_serv.conf

4. Go to Administer->Site building->Modules (?q=admin/build/modules), and enable the shoutcast.module

5. Go to Administer->Site configuration->Modules (?q=admin/settings/shoutcast), and provide the default settings

6. Go to Administer->User management->Access control (?q=admin/user/access) to modify the access resctrictions

7. Go to Create content->shoutcast (?q=node/add/shoutcast) to create a new server

Comments and suggestions to
Stefan Auditor <stefan.auditor@erdfisch.de>

Please report any bugs on
http://drupal.org/project/issues/shoutcast