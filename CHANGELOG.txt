
CHANGELOG
---------


shoutcast.module - October, 7th 2010
-------------------------------------
-Major change to configuration settings, moved to block configuration
-Implement change request for multiblock w/ seperate settings
-Added permission for detail link display to enforce page permissions


shoutcast.module - October, 5th 2010
-------------------------------------
-Module suggestions by thumb all statistics from shoutcast server

shoutcast.module - March, 20th 2007
--------------------------------------
- Drupal 5 compatibility update.

shoutcast.module - October, 17th 2006
--------------------------------------
- initial release.

Comments and suggestions Welcome
