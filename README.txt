
README
------

This module adds shoutcast statistcs block to your Website.

"SHOUTcast is Nullsoft's Free Winamp-based distributed streaming audio system. Thousands of broadcasters around the world are waiting for you to tune in and listen."
source: http://shoutcast.com/

This module is only tested with the linux-version of shoutcast "SHOUTcast Linux server (glibc) v1.9.8".


Please report any bugs on
http://drupal.org/project/issues/shoutcast